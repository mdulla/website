import "./App.css";
import Banner from "./components/common/Banner";
import Footer from "./components/common/Footer";

import Navigation from "./components/common/Navigation";

import Fourcolumns from "./components/Dependencies/Loginpage/Fourcol";
import Inner_wrapper from "./components/Dependencies/Loginpage/Innerwrapper";
import One_third from "./components/Dependencies/Loginpage/Innerwrapper1";

/**
 * This is a Function representing the main page of the application.
 */
function Output() {
  /**
   * Object for Footer Section
   * Passing link properties
   */

  var Apps = [
    { link: "fa fa-facebook" },
    { link: "fa fa-google-plus" },
    { link: "fa fa-twitter" },
    { link: "fa fa-youtube" },
    { link: "fa fa-instagram" },
  ];
  /**
   * Object for FourColumn Section
   * Passing id,icon ,name object properties
   */

  var shortcuts = [
    { icon: "fa fa-html5", id: "html", name: "HTML" },
    { icon: "fa fa-css3", id: "css", name: "CSS" },
    { icon: "fa fa-search", id: "seo", name: "SEO" },
    { icon: "fa fa-users", id: "social", name: "Social" },
  ];
  /**
   * Object for Inner_wrapper Section
   * Passing artid,id,imglink,text object properties by using props
   */

  var shortcut = [
    {
      artid: "tablet",
      id: "tablet2",
      imglink:
        "https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571257/hand_ipad_vr0pfu.png",
      text:
        " A clean HTML layout and CSS stylesheet making for a great responsive" +
        " framework to design around that includes a responsive drop down" +
        " navigation menu, image slider, contact form and ‘scroll to the top" +
        " jQuery plugin.jQuery plugin.that includes a responsive drop down" +
        "navigation menu, image slider, contact form and ‘scroll to the top’" +
        " jQuery plugin.",
      name: "Mobile.Tablet.Desktop",
      sectionclass: "inner-wrapper",
    },
    {
      artid: "mobile",
      id: "hand-mobile",
      imglink:
        "https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571257/hand_mobile_stt5sh.png",
      text:
        " A clean HTML layout and CSS stylesheet making for a great responsive" +
        " framework to design around that includes a responsive drop down" +
        " navigation menu, image slider, contact form and ‘scroll to the top" +
        " jQuery plugin.jQuery plugin.that includes a responsive drop down" +
        "navigation menu, image slider, contact form and ‘scroll to the top’" +
        " jQuery plugin.",
      name: "Across each device",
      sectionclass: "inner-wrapper-2",
    },
    {
      artid: "",
      id: "desktop",
      imglink:
        "https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571256/desktop_fsksnx.png",
      text:
        " A clean HTML layout and CSS stylesheet making for a great responsive" +
        " framework to design around that includes a responsive drop down" +
        " navigation menu, image slider, contact form and ‘scroll to the top" +
        " jQuery plugin.jQuery plugin.that includes a responsive drop down" +
        "navigation menu, image slider, contact form and ‘scroll to the top’" +
        " jQuery plugin.",
      name: "Desktop",
      sectionclass: "inner-wrapper",
    },
  ];
  /**
   * Object for one_third Section
   * Passing id,name object ,paragraph properties *
   */

  var Inner = [
    {
      id: "google",
      name: "Google Search",
      paragraph:
        " Also included with the Template is the Template Customization Guide" +
        "with five special video lessons showing you how to get professional" +
        "website pictures & how to edit them to fit the template,flessons" +
        " showing you how to get professional website pictures & how to edit" +
        "them to fit the template,f",
    },

    {
      id: "marketing",
      name: "Marketing",
      paragraph:
        "Note: this template includes a page with a PHP website contact form" +
        "and requires a web host or a program such as XAMPP to run PHP and" +
        "display it’s content.id= desktop lessons showing you how to get" +
        "professional website pictures & how to edit them to fit the template",
    },
    {
      id: "customers",
      name: "Happy Customers",
      paragraph:
        "When you purchase and download The Rocket Design HTML Template you" +
        "get a full five page responsive HTML website template with both a" +
        "“light” and “dark” version of the template in addition to the" +
        "following features:",
    },
  ];
  /**
   * Represents the all components
   */

  return (
    <>
      <Navigation />
      <Banner />
      <Fourcolumns login_four={shortcuts} />
      <Inner_wrapper login_inner={shortcut} />
      <One_third login_inner1={Inner} />
      <Footer login_footer={Apps} />
    </>
  );
}
// will be exposed globally as Component
export default Output;
