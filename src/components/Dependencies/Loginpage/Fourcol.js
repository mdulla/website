/**
 * Represents the one-fourth section
 * Used props to get the data from Main Parent component(App.js)
 */

function Fourcolumns(props) {
  return (
    <>
      {props.login_four.map((data) => {
        return (
          <div>
            <section className="one-fourth" id={data.id} key={data.id}>
              <td>
                <i className={data.icon}></i>
              </td>
              <h3>{data.name}</h3>
            </section>
          </div>
        );
      })}
      ;
    </>
  );
}
// will be exposed globally as Component

export default Fourcolumns;
