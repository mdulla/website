/**
 * Represents the One third section
 * @param {*} props
 */

function Inner_wrapper(props) {
  return (
    <>
      {props.login_inner.map((data) => {
        if (data.sectionclass == "inner-wrapper") {
          return (
            <FloatingBanner
              sectionclass={data.sectionclass}
              imglink={data.imglink}
              id={data.id}
              name={data.name}
              text={data.text}
              artid={data.artid}
              key={data.sectionclass}
            />
          );
        } else {
          return (
            <FloatingBannerleft
              sectionclass={data.sectionclass}
              imglink={data.imglink}
              id={data.id}
              name={data.name}
              text={data.text}
              artid={data.artid}
            />
          );
        }
      })}
    </>
  );
}
function FloatingBanner(props) {
  return (
    <>
      <section className={props.sectionclass}>
        <article id={props.artid}>
          <img src={props.imglink} />
        </article>
        <aside id={props.id}>
          <h2>{props.name}</h2>
          <p>{props.text}</p>
        </aside>
      </section>
    </>
  );
}
function FloatingBannerleft(props) {
  return (
    <>
      <section className={props.sectionclass}>
        <aside id={props.id}>
          <img src={props.imglink} />
        </aside>
        <article id={props.artid}>
          <h2>{props.name}</h2>
          <p>{props.text}</p>
        </article>
      </section>
    </>
  );
}
// will be exposed globally as Component
export default Inner_wrapper;
