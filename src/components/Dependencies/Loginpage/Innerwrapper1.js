/**
 * Represents the One third section
 * @param {*} props
 */

function One_third(props) {
  
  function Card(props) {
    return (
      <section className="one-third" id={props.id}>
        <h3>{props.name}</h3>
        <p>{props.paragraph}</p>
      </section>
    );
  }
  return (
    <>
      <section className="inner-wrapper-3">
        {props.login_inner1.map((data) =>{
          return <Card head={data.name} text={data.paragraph} id={data.id}  key={data.name}/>;
        })}
        <section id="smelly">
          <h2>: )</h2>
        </section>
      </section>
    </>
  );
}
// will be exposed globally as Component
export default One_third