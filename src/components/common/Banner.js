import React from "react";
function Banner() {
  return (
    <>
      <section className="banner">
        <div className="banner-inner">
          <img
            src="https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571172/rocket_design_k4nzbm.png"
            alt=""
          />
        </div>
      </section>
      {/* <!--end of banner--> */}
    </>
  );
}
export default Banner;
