/**
 * Represents the First footer section
 * This section has social links
 */

function Footer(props) {
  return (
    <>
      <footer>
        <ul className="social">
          {props.login_footer.map((image) => {
            return <Iconsimg img={image.link} />;
          })}
        </ul>
      </footer>
      <footer className="second">
        <p>&copy; MaxPower Design</p>
      </footer>
    </>
  );

  function Iconsimg(props) {
    return (
      <>
        <li>
          <a href="/" target="_blank">
            <i className={props.img}></i>
          </a>
        </li>
      </>
    );
  }
}

export default Footer;
