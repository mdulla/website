/**
 * Represents the Navbar of the website
 */


import React from "react";
function Navigation() {
   let information = [
    {data: "skills"},
    {data: "Portfolio"},
    {data: "Our team"},
    {data: "contact"}
  ];
  return (
    <>
      <div id="header-inner">
        <a href="/" id="logo"></a>
        <nav>
          <a href="/" id="menu-icon"></a>
          <ul>
            <li>
              <a href="/" className="current">
                Home
              </a>
            </li>
            <li>
              <a href="/">Skills</a>
            </li>
            <li>
              <a href="/">Portfolio</a>
            </li>
            <li>
              <a href="/">Our team</a>
            </li>
            <li>
              <a href="/">Contact</a>
            </li>
          </ul>
        </nav>
      </div>
    </>
  );
}
// will be exposed globally as Component
export default Navigation;
